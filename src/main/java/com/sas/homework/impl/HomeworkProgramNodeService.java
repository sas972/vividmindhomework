package com.sas.homework.impl;

import java.util.Locale;

import com.ur.urcap.api.contribution.ViewAPIProvider;
import com.ur.urcap.api.contribution.program.ContributionConfiguration;
import com.ur.urcap.api.contribution.program.CreationContext;
import com.ur.urcap.api.contribution.program.ProgramAPIProvider;
import com.ur.urcap.api.contribution.program.swing.SwingProgramNodeService;
import com.ur.urcap.api.domain.data.DataModel;

public class HomeworkProgramNodeService implements SwingProgramNodeService<HomeworkProgramNodeContribution, HomeworkProgramNodeView> {

	@Override
	public String getId() {
		return "HomeworkProgramNode";
	}

	@Override
	public void configureContribution(ContributionConfiguration configuration) {
		configuration.setChildrenAllowed(false);
		configuration.setUserInsertable(true);		
	}

	@Override
	public String getTitle(Locale locale) {
		return "Program Node";
	}

	@Override
	public HomeworkProgramNodeView createView(ViewAPIProvider apiProvider) {
		return new HomeworkProgramNodeView(apiProvider);
	}

	@Override
	public HomeworkProgramNodeContribution createNode(ProgramAPIProvider apiProvider, HomeworkProgramNodeView view,
			DataModel model, CreationContext context) {
		return new HomeworkProgramNodeContribution(apiProvider, view, model, context);
	}

}
