package com.sas.homework.impl;

import com.ur.urcap.api.contribution.ProgramNodeContribution;
import com.ur.urcap.api.contribution.program.CreationContext;
import com.ur.urcap.api.contribution.program.ProgramAPIProvider;
import com.ur.urcap.api.domain.data.DataModel;
import com.ur.urcap.api.domain.script.ScriptWriter;
import com.ur.urcap.api.domain.undoredo.UndoRedoManager;
import com.ur.urcap.api.domain.undoredo.UndoableChanges;

public class HomeworkProgramNodeContribution implements ProgramNodeContribution {

	private final static String SLIDER_KEY = "slider";
	private final static String FORCE_KEY = "force";
	private final static String VIEWSTATE_KEY = "viewstate";
	
	
	private final ProgramAPIProvider apiProvider; 
	private final HomeworkProgramNodeView view;
	private final DataModel model;
	private final CreationContext context;
	private final UndoRedoManager undoRedoManager;
	
	
	public HomeworkProgramNodeContribution(ProgramAPIProvider apiProvider, HomeworkProgramNodeView view,
			DataModel model, CreationContext context) {
		super();
		this.apiProvider = apiProvider;
		this.view = view;
		this.model = model;
		this.context = context;
		undoRedoManager = apiProvider.getProgramAPI().getUndoRedoManager();
	}
	
	
	public void saveSlider( final int value ) {
		undoRedoManager.recordChanges(new UndoableChanges() {
			
			@Override
			public void executeChanges() {
				model.set(SLIDER_KEY, value);				
			}
		});
	}

	public void saveForce( final int value ) {
		undoRedoManager.recordChanges(new UndoableChanges() {
			
			@Override
			public void executeChanges() {
				model.set(FORCE_KEY, value);				
			}
		});
	}

	public void saveViewState( final boolean value ) {
		undoRedoManager.recordChanges(new UndoableChanges() {
			
			@Override
			public void executeChanges() {
				model.set(VIEWSTATE_KEY, !value);				
			}
		});
	}

	@Override
	public void openView() {
		view.setForce(getForce());
		view.setSlider(model.get(SLIDER_KEY, 1));
		view.setViewState(model.get(VIEWSTATE_KEY, true));

	}


	private int getForce() {
		return model.get(FORCE_KEY, 0);
	}

	@Override
	public void closeView() {

	}

	@Override
	public String getTitle() {
		return "Program Node";
	}

	@Override
	public boolean isDefined() {
		return view.checkForceRange(getForce());
	}

	@Override
	public void generateScript(ScriptWriter writer) {
		// TODO Auto-generated method stub

	}

}
