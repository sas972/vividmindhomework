package com.sas.homework.impl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.ur.urcap.api.contribution.ContributionProvider;
import com.ur.urcap.api.contribution.ViewAPIProvider;
import com.ur.urcap.api.contribution.program.swing.SwingProgramNodeView;

public class HomeworkProgramNodeView implements SwingProgramNodeView<HomeworkProgramNodeContribution>, 
													ActionListener, ChangeListener {

	private static final String CONTACT_FORCE = "Contact force: ";

	private static final String _5_25 = "5-25";

	private final ViewAPIProvider apiProvider;
	
	private JPanel innerPanel = new JPanel();
	private JButton theButton = new JButton();
	
	private JSlider slider = new JSlider(1, 25);
	private JTextField forceTextField = new JTextField(_5_25);
	private ImageIcon outIcon;
	private ImageIcon inIcon;
	boolean inSliderState = true;

	private JLabel sliderLabel = new JLabel(CONTACT_FORCE);
	
	ContributionProvider<HomeworkProgramNodeContribution> provider;
	
	
	public HomeworkProgramNodeView(ViewAPIProvider apiProvider) {
		super();
		this.apiProvider = apiProvider;
	}


	@Override
	public void buildUI(JPanel panel, ContributionProvider<HomeworkProgramNodeContribution> provider) {
		this.provider = provider;
		innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.Y_AXIS));
		outIcon = createIcon("/out.png");
		inIcon = createIcon("/in.png");
		theButton.addActionListener(this);
		initForceTextField(provider);
		slider.addChangeListener(this);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));	
		panel.add(createSpacer(20));
		panel.add(createMainBox());
		switchView();
	}

	public void setForce( int force ) {
		if(checkForceRange(force))
			forceTextField.setText(Integer.toString(force));
		else
			forceTextField.setText("");
		checkForceTextField();
	}
	
	public void setSlider(int value) {
		slider.setValue(value);
		setSliderLabel();
	}

	
	public void setViewState( boolean value ) {
		inSliderState = value;
		switchView();
	}
	
	
	private void initForceTextField(final ContributionProvider<HomeworkProgramNodeContribution> cProvider) {
		forceTextField.setPreferredSize(new Dimension(60, 30));
		forceTextField.setMaximumSize(forceTextField.getPreferredSize());
		forceTextField.setForeground(Color.gray);
		forceTextField.setHorizontalAlignment(JTextField.CENTER);
		forceTextField.setToolTipText(_5_25);
		forceTextField.setInputVerifier(new InputVerifier() {
			
			@Override
			public boolean verify(JComponent input) {
				return validateForceTextField((JTextField) input);
			}

		});
		forceTextField.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				handleUpdate();
				
			}

			private void handleUpdate() {
				if(checkForceTextField())
					cProvider.get().saveForce(Integer.parseInt(forceTextField.getText()));
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				handleUpdate();				
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				handleUpdate();				
			}
		});
	}
	
	private boolean validateForceTextField(JTextField textField) {
		String text = textField.getText();
		int value = 0;
		try {
			value = Integer.parseInt(text);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return checkForceRange(value);
	}


	public boolean checkForceRange(int value) {
		return 4 < value  && 26 > value;
	}
	
	private Component createSpacer(int height) {
		return Box.createRigidArea(new Dimension(0, height));
	}

	private Box createMainBox() {
		Box box = Box.createHorizontalBox();
		box.setAlignmentX(Component.LEFT_ALIGNMENT);
		box.add(theButton);
		
		if ( null != outIcon) {
			theButton.setIcon(outIcon);
		}
		box.add(Box.createRigidArea(new Dimension(10, 0)));
		box.add(innerPanel);
		return box;
	}

	private void buildInnerPanelWJText() {
		innerPanel.removeAll();
		innerPanel.add(createJTextLabelBox());
		innerPanel.add(createForceTaxtBox());
	}


	private Box createForceTaxtBox() {
		Box box  = Box.createHorizontalBox();
		box.setAlignmentX(Component.LEFT_ALIGNMENT);
		box.add(forceTextField);
		box.add(new JLabel(" N"));
		return box;
	}
	
	private Box createJTextLabelBox() {
		Box box = Box.createHorizontalBox();
		box.setAlignmentX(Component.LEFT_ALIGNMENT);
		box.add(new JLabel("Force:"));
		return box;
	}
	
	private void buildInnerPanelWSlider() {
		innerPanel.removeAll();
		innerPanel.add(createSliderLabelBox());
		innerPanel.add(createSliderBox());
	}


	private Box createSliderBox() {
		Box box  = Box.createHorizontalBox();
		box.setAlignmentX(Component.CENTER_ALIGNMENT);
		box.add(new JLabel("Soft"));
		box.add(slider);
		box.add(new JLabel("Strict"));
		return box;
	}


	private Box createSliderLabelBox() {
		Box box = Box.createHorizontalBox();
		box.setAlignmentX(Component.CENTER_ALIGNMENT);
		box.add(sliderLabel);
		return box;
	}

	private ImageIcon createIcon(String imagepath) {
		ImageIcon icon = null;
		Image image = readImage(imagepath);
		if(null != image) {
			icon = new ImageIcon(image);
		}
		return icon;
	}


	private Image readImage(String imagepath) {
		Image image = null;
		try {
			image = ImageIO.read(getClass().getResource(imagepath));
		} catch (IOException e) {
			// TODO ?????
			e.printStackTrace();
		}
		return image;
	}

	

	@Override
	public void actionPerformed(ActionEvent e) {
		switchView();
		provider.get().saveViewState(inSliderState);
	}


	private void switchView() {
		if (inSliderState) {
			buildInnerPanelWJText();
			if(null != outIcon) theButton.setIcon(outIcon);
			inSliderState = false;
		} else {
			buildInnerPanelWSlider();
			if(null != inIcon) theButton.setIcon(inIcon);
			inSliderState = true;
		}
		innerPanel.revalidate();
		innerPanel.paintAll(innerPanel.getGraphics());
	}

	private boolean checkForceTextField() {
		if(validateForceTextField(forceTextField)) {
			forceTextField.setForeground(Color.BLACK);
			return true;
		} else {
			forceTextField.setForeground(Color.RED);
			return false;
		}
	}


	@Override
	public void stateChanged(ChangeEvent arg0) {
		setSliderLabel();
		provider.get().saveSlider(slider.getValue());
	}


	private void setSliderLabel() {
		sliderLabel.setText(CONTACT_FORCE + slider.getValue() + " N");
	}
}
